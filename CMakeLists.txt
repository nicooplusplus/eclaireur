 #
 # This program is free software; you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation; either version 2 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program; if not, write to the Free Software
 # Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 # MA 02110-1301, USA.
 #
 #
 #
  
cmake_minimum_required( VERSION 2.8 )
project( Eclaireur )



SET ( CMAKE_VERBOSE_MAKEFILE ON )
SET ( SOURCES src/argparse.cc src/engine.cc src/GTKMM_engine.cc 
              src/WEBKIT2_engine.cc src/application.cc src/main.cc )
SET ( EXECUTABLE_NAME Eclaireur )



find_package( Boost COMPONENTS system filesystem      REQUIRED )


find_package( PkgConfig REQUIRED )
pkg_search_module ( GTKMM REQUIRED gtkmm-3.0 )
pkg_search_module ( WEBKIT2GTK REQUIRED webkit2gtk-4.0)

macro( config_project PROJNAME LIBNAME )
include_directories( ${${LIBNAME}_INCLUDEDIR} )
include_directories( ${${LIBNAME}_INCLUDE_DIRS} )
target_link_libraries( ${PROJNAME} ${${LIBNAME}_LIBRARIES} )
endmacro()


add_executable ( ${EXECUTABLE_NAME} ${SOURCES} )

add_definitions(-std=c++11)

target_link_libraries(${EXECUTABLE_NAME}
  ${Boost_FILESYSTEM_LIBRARY}
  ${Boost_LIBRARIES}
)

config_project(${EXECUTABLE_NAME} GTKMM )
config_project(${EXECUTABLE_NAME} WEBKIT2GTK )
