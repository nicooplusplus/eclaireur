#include <map>
#include <gtkmm.h>
#include <string>
#include <cstring>
#include <unistd.h>
#include "GTKMM_engine.hh"
#include "WEBKIT2_engine.hh"

#define __APPLICATION_NAME__ "Eclaireur.Web.browser"
#define CAST(T,X) static_cast< T * >(widgets[X])
#define CONNECT(X) connect( sigc::mem_fun( this, &GTKMM_engine::X ) );

GTKMM_engine::GTKMM_engine( Application* appli
                          , bool simpleViewer
                          )
: engine(appli)
, currentPage(0)
{
  app = Gtk::Application::create( __APPLICATION_NAME__ );


  /*main window*/
  Gtk::Window * W1 = new Gtk::Window();
  
  W1->set_default_size(600, 400);
  
  W1->set_title("Eclaireur");
  
  W1->add_events(Gdk::KEY_PRESS_MASK|Gdk::KEY_RELEASE_MASK);
  
  W1->signal_key_press_event().CONNECT( onKeyPressed );
  
  W1->signal_key_release_event().CONNECT( onKeyReleased );
  
  widgets["main window"] = W1;

  if(simpleViewer)
    return;


  /*main box*/
  Gtk::Box * X1 
               = new Gtk::Box( Gtk::Orientation::ORIENTATION_VERTICAL );
               
  widgets["main box"] = X1;
  
  
  /*toolbar*/
  Gtk::Toolbar * T1 = new Gtk::Toolbar();
  
  widgets["toolbar"] = T1;
  

  /*tabs bar*/
  Gtk::Toolbar * T2 = new Gtk::Toolbar();
  
  widgets["tabs bar"] = T2;
  
  
  /*back button*/
  Gtk::ToolButton * B1 = new Gtk::ToolButton(Gtk::Stock::GO_BACK);
  
  B1->signal_clicked().CONNECT( backButton );

  widgets["back"] = B1;
  
  
  /*forward button*/
  Gtk::ToolButton * B2 = new Gtk::ToolButton(Gtk::Stock::GO_FORWARD);
  
  B2->signal_clicked().CONNECT( forwardButton );
  
  widgets["forward"] = B2;
  
  
  /*the custom button, needed to nest the entry*/
  Gtk::ToolItem * I1 = new Gtk::ToolItem();
  
  I1->set_halign(Gtk::ALIGN_FILL);
  I1->set_expand (); // 4.14

  widgets["toolitem"] = I1;
  
  
  /*entry*/
  Gtk::Entry * E1 = new Gtk::Entry();
  
  E1->signal_icon_press().CONNECT( entry1Icon2Pressed );
  
  E1->signal_activate().CONNECT( entryEnterPressed );
  
  E1->set_halign(Gtk::ALIGN_FILL);
  
  widgets["entry"] = E1;


  /*ok*/
  Gtk::ToolButton * B3 = new Gtk::ToolButton(Gtk::Stock::OK);
  
  B3->signal_clicked().CONNECT( okButton );
  
  widgets["ok"] = B3;
  
  
  /*switcher find/uri*/
  Gtk::ToggleToolButton * B4 
                          = new Gtk::ToggleToolButton(Gtk::Stock::FIND);

  B4->signal_clicked().CONNECT( switcherButton );
  
  widgets["switcher"] = B4;
  
  
  /*search engine*/
  Gtk::ToolButton * B5 
                     = new Gtk::ToolButton(Gtk::Stock::DIALOG_QUESTION);
  
  B5->signal_clicked().CONNECT( searchEngineButton );
  
  widgets["search engine"] = B5;

  /*the main stack*/
  Gtk::Stack * S1 = new Gtk::Stack();
  
  widgets["main stack"] = S1;
  
  
  /*the web notebook*/
  Gtk::Notebook * N1 = new Gtk::Notebook();
  
  widgets["webviews notebook"] = N1;
  
  N1->signal_switch_page().CONNECT( pageSwitched );

  N1->set_show_tabs(false);
  
  /**/
  
  T1->append( *B1 );
  T1->append( *B2 );
  T1->append( *I1 );
  T1->append( *B3 );
  T1->append( *B4 );
  T1->append( *B5 );
  
  I1->add( *E1);
  
  X1->add( *T1 );
  X1->add( *T2 );
  X1->add( *S1 );

  S1->add( *N1, "webviews notebook"); 
  
  W1->add( *X1 );
  W1->show_all();
  
}

#define N1 CAST(Gtk::Notebook,"webviews notebook")

GTKMM_engine::~GTKMM_engine(  )
{
  for (const auto& i : widgets)
    delete i.second;
}

bool
GTKMM_engine::startUI(  )
{
  app->run( *CAST(Gtk::Window,"main window") );
}

void
GTKMM_engine::addWebkit( GtkWidget * WebViewGTK
                       , bool focus
                       )
{ 
  Gtk::Widget* _WebViewGTK = Glib::wrap( WebViewGTK );
  
  _WebViewGTK->set_hexpand( true );
  
  _WebViewGTK->set_vexpand( true );
  
  /// ***********************************************************************FIX ME SIMPLE VIEWER
  //~ if( WindowList.begin()->first == "window2" )
  //~ {
    //~ WindowList.begin()->second->add( *_WebViewGTK );
    //~ _WebViewGTK->show();
    //~ return;
  //~ }
  
  // the next two lines need to be called before append_page()
  URIList.push_back(""); 
  
  SearchList.push_back("");
  
  N1->append_page( *_WebViewGTK, "" );
  _WebViewGTK->show();

  if( N1->get_n_pages() > 1 && !N1->get_show_tabs() )
    N1->set_show_tabs(true);
}


bool
GTKMM_engine::saveAFile( std::string & destination
                       , std::string filename ) // won't be returned 
                                                //               anyway.
{ 
  std::cout << "no download! FCW is a nullptr!\n" << std::endl;
  return false;
    std::cout<<"savefile [begin]"<<std::endl;
  //FCD->set_current_name( filename );
  FCW->set_current_name( filename );
  
  //FCD->set_current_folder("/home/user");
  FCW->set_current_folder("/home/user");
  
  FCW->show();
  //int i = FCD->run();

  fileChooser = true;

  //FCD->close();
  while(fileChooser)
  {
    Glib::usleep( 50000 );
    std::cout << 1 << std::endl;
    while( Gtk::Main::events_pending() )
    Gtk::Main::iteration();
  }  
  std::cout << "t" << std::endl;
  
  if( save ) // ok button has been clicked
  {
    //destination = FCD->get_filename();
    destination = FCW->get_filename();

    return true;
  }
  
  return false; // cancel button has been necessarily clicked
}

void
GTKMM_engine::setTitle( GtkWidget* WebViewGTK
                      , Glib::ustring title )
{
  Gtk::Widget* WebView = Glib::wrap( WebViewGTK );
  
  if( title.size() > 20 )
    title.erase( 20 );
 	
   N1->set_tab_label_text( *WebView, title );
}

void
GTKMM_engine::setEntryURI( std::string uri
                         , int webviewID )
{
  URIList[webviewID] = uri;

  if( webviewID == currentPage )
    CAST(Gtk::Entry,"entry")->set_text( URIList[currentPage] ); 
}

void
GTKMM_engine::entryEnterPressed(  )
{
  if( CAST(Gtk::ToggleToolButton,"switcher")->get_active() )
  {
    eWEBKIT2->search( 
           (CAST(Gtk::Entry,"entry")->get_text()).c_str(), currentPage);
  }
  else
  {
    if( keyPressed == 65293 )
    {
      eWEBKIT2->addANewWebView( "yahoo.fr" );
      std::cout << 1 << std::endl;
    }
    else
    {
      std::cout << 2 << std::endl;
      eWEBKIT2->loadURI( CAST(Gtk::Entry,"entry")->get_text() ,currentPage );
      //~ eWEBKIT2->addANewWebView( "google.fr" );
    }
  }
}

void
GTKMM_engine::backButton(  )
{
  if(eWEBKIT2->matching() )
  {
    eWEBKIT2->previousFound();
  }
  else
  {
    eWEBKIT2->goBack(currentPage);
  }
}

void
GTKMM_engine::forwardButton(  )
{
  if(eWEBKIT2->matching() )
  {
    eWEBKIT2->nextFound();
  }
  else
  {
    eWEBKIT2->goForward(currentPage);
  }
}


void
GTKMM_engine::okButton(  )
{
  if( modifierKeyPressed == GDK_CONTROL_MASK )
  {
    std::cout << "test" << std::endl;
    //~ eWEBKIT2->addANewWebView( "" );
  }
  else
  {
    if( CAST(Gtk::ToggleToolButton,"switcher")->get_active() )
    {
      eWEBKIT2->search( std::string(CAST(Gtk::Entry,"entry")
                                  ->get_text()).c_str() , currentPage );
    }
    else
    {
      eWEBKIT2->loadURI( CAST(Gtk::Entry,"entry")
                                            ->get_text(), currentPage );
    }
  }
}

void
GTKMM_engine::switcherButton(  )
{
  if( CAST(Gtk::ToggleToolButton,"switcher")->get_active() )
  {
    URIList[currentPage] = CAST(Gtk::Entry,"entry")->get_text();
    
    CAST(Gtk::Entry,"entry")->set_text( SearchList[currentPage] );
  }
  else
  {
    SearchList[currentPage] = CAST(Gtk::Entry,"entry")->get_text();
    
    CAST(Gtk::Entry,"entry")->set_text( URIList[currentPage] );
    
    eWEBKIT2->endSearch();
  }
}
void

GTKMM_engine::searchEngineButton(  )
{
  std::cout << "search engine button" << std::endl;
  std::string searchEngineURI = "https://duckduckgo.com/?q=";
  
  std::string URI = searchEngineURI + CAST(Gtk::Entry,"entry")
                                                           ->get_text();
  
  if( CAST(Gtk::ToggleToolButton,"switcher")->get_active() )
    CAST(Gtk::ToggleToolButton,"switcher")->set_active( false );
    
  if( modifierKeyPressed == GDK_CONTROL_MASK )
    eWEBKIT2->addANewWebView( URI );
  else
    eWEBKIT2->loadURI( URI, currentPage );
}

void
GTKMM_engine::entry1Icon2Pressed( int entryIconPosition
                                , const GdkEventButton * event )
{ 
  if( entryIconPosition == 1 )
    CAST(Gtk::Entry,"entry")->set_text( "" );
}

void
GTKMM_engine::pageSwitched( Gtk::Widget * page
                          , int page_num )
{
  if( CAST(Gtk::ToggleToolButton,"switcher")->get_active() )
    CAST(Gtk::ToggleToolButton,"switcher")->set_active(false);
  currentPage = page_num;
  CAST(Gtk::Entry,"entry")->set_text( URIList[currentPage] );
}

bool
GTKMM_engine::onKeyPressed( GdkEventKey * event )
{
  //~ std::cout << "key pressed" << std::endl;
  // GDK_CONTROL_MASK
  keyPressed = event->keyval;
  
  modifierKeyPressed = event->state;
  
  //~ std::cout << modifierKeyPressed;
  
  //~ if( modifierKeyPressed)
    //~ std::cout << " modifier key pressed" << std::endl;
  //~ else
    //~ std::cout << " no modifier key pressed" << std::endl;
  
  return true;
}

bool
GTKMM_engine::onKeyReleased( GdkEventKey * event )
{
  //~ std::cout << "key released" << std::endl;
  keyPressed = event->keyval;
  
  modifierKeyPressed = event->state;
  //~ std::cout << modifierKeyPressed;
  
  //~ if( modifierKeyPressed )
    //~ std::cout << " modifier key released" << std::endl;
  //~ else
    //~ std::cout << " no modifier key released" << std::endl;
  
  return true;
}


void
GTKMM_engine::okClicked(  )
{
  save = true;
  
  fileChooser = false;
}

void
GTKMM_engine::cancelClicked(  )
{
  save = false;
  
  fileChooser = false;
}
