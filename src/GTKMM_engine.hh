#ifndef __GTKMM_ENGINE_H_INCLUDED__
#define __GTKMM_ENGINE_H_INCLUDED__
#include "engine.hh"

class
GTKMM_engine : public engine
{
  public:
  GTKMM_engine(Application*,bool);
  ~GTKMM_engine();
  bool startUI();
  void addWebkit(GtkWidget*,bool);
  bool saveAFile(std::string&,std::string);
  void setTitle(GtkWidget*,Glib::ustring);
  void setEntryURI(std::string, int);
  void entryEnterPressed();

  private:
  Glib::RefPtr< Gtk::Application > app;
  Glib::RefPtr< Gtk::Builder > refBuilder;
  std::map < const std::string, Gtk::Window * >  WindowList;
  std::map < const std::string, Gtk::ToolButton * > ToolButtonList;
  std::map < const std::string, Gtk::ToggleToolButton * > ToggleToolButtonList;
  std::map < const std::string, Gtk::Entry * > EntryList;
  std::map < const std::string, Gtk::Box * > BoxList;
  std::map < const std::string, Gtk::Paned * > PanedList;
  std::map < const std::string, Gtk::Notebook * > NotebookList;
  std::map < const std::string, Gtk::Widget * >  widgets;
  void backButton();
  void forwardButton();
  void okButton();
  void switcherButton();
  void searchEngineButton();
  void entry1Icon2Pressed(int,const GdkEventButton*);
  void pageSwitched(Gtk::Widget*, int);
  bool onKeyPressed(GdkEventKey*);
  bool onKeyReleased(GdkEventKey*);
  guint keyPressed;
  guint modifierKeyPressed;
  std::vector <std::string> URIList;
  std::vector <std::string> SearchList;
  int currentPage;
  
  Gtk::FileChooserDialog * FCD;
  Gtk::Button * ok;
  Gtk::Button * cancel;
  
  Gtk::StackSwitcher * StSw;
  Gtk::Stack * St;
  Gtk::FileChooserWidget * FCW;
  
  Gtk::Button * Ok;
  Gtk::Button * Cancel;
  Gtk::ButtonBox * BuBo;
  void okClicked();
  void cancelClicked();
  bool fileChooser;
  bool save;
};

#endif // __GTKMM_ENGINE_H_INCLUDED__
