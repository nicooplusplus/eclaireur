/*
 * application.hh
 * 
 * Copyright 2013-2015 Jäger Nicolas
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
#ifndef __APPLICATION_H_INCLUDED__
#define __APPLICATION_H_INCLUDED__

#include <map>
#include "argparse.hh"

class GTKMM_engine;
class WEBKIT2_engine;

class Application{
public:
	Application(const InitData*);
	~Application();
	bool still_running;
  void run();
  void stop();
  const InitData *parsed;
  
  int getWidth(); // change for get resolution...
  int getHeight();
  void setResolution(const int,const int);
  
private:
	GTKMM_engine* eGTKMM;
  WEBKIT2_engine* eWEBKIT2;
  int width, height; // current width and height of the application

};


#endif // __APPLICATION_H_INCLUDED__
