#include "WEBKIT2_engine.hh"
#include <iostream>
#include <webkit2/webkit2.h>
#include <string>
#include "GTKMM_engine.hh"

static
void
foundTextGC( WebKitFindController * findController
           , guint matchCount
           , bool * matched
           )
{
  if( matchCount > 0 )
    *matched = true;
  else
    *matched = false;
}

static
void
notifyTitleGC( GObject * gobject
             , GParamSpec * pspec
             , WEBKIT2_engine * THIS
             )
{
  THIS->get_GTKMMengine()->setTitle( GTK_WIDGET(gobject)
              , webkit_web_view_get_title( WEBKIT_WEB_VIEW(gobject) ) );
}

static
void
loadChangedGC( WebKitWebView  * view
             , WebKitLoadEvent loadEvent
             , WEBKIT2_engine * THIS
             )
{
  switch( loadEvent )
  {
  case WEBKIT_LOAD_STARTED:
  case WEBKIT_LOAD_REDIRECTED:
  case WEBKIT_LOAD_COMMITTED:  
    THIS->get_GTKMMengine()->setEntryURI(
                                    webkit_web_view_get_uri ( view )
                                  , THIS->getWebViewIndex( view ) );
    break;
  case WEBKIT_LOAD_FINISHED:
    break;
  }
}

void
decidedDestinationGC( WebKitDownload * download
                    , gchar * suggestedFilename
                    , WEBKIT2_engine * THIS
                    )
{
  std::string destination;
  
  bool result = THIS->get_GTKMMengine()->saveAFile( destination,
                                       std::string(suggestedFilename) );
  
  
  
  if(result)
  {
    destination = "file://"+destination;
  
    webkit_download_set_destination ( download, destination.c_str() );
  }
  else
    webkit_download_cancel( download );
}

void
failedGC( WebKitDownload * download
        , gpointer error
        , gpointer userData
        )
{
  std::cout << "failedGC" << std::endl;
  
  GError *e = static_cast<GError*>(error);
  
  std::cout << e->message << std::endl;
}

void
receivedDataGC( WebKitDownload * download
              , guint64 dataLength
              , gpointer userData
              )
{
  //~ std::cout << "receivedDataGC" <<std::endl;
}

void
finishedGC( WebKitDownload * download
          , gpointer userData
          )
{
  //~ std::cout << "finishedGC" << std::endl;
}

void
createdDestinationGC( WebKitDownload * download
                    , gpointer userData
                    )
{
  //~ std::cout << "createdDestinitationGC" << std::endl;
  //~ std::cout << webkit_download_get_destination( download ) << std::endl;
}

static
void
downloadRequestedGC( WebKitWebContext * context
                   , WebKitDownload * download
                   , WEBKIT2_engine * THIS
                   )
{
  std::cout << "downloadRequestedGC" << std::endl;

  g_signal_connect( download, "created-destination"
                          , G_CALLBACK(createdDestinationGC), nullptr );
                          
  g_signal_connect( download, "finished"
                                    , G_CALLBACK(finishedGC), nullptr );
                                    
  g_signal_connect( download, "failed"
                                      , G_CALLBACK(failedGC), nullptr );
                                      
  g_signal_connect( download, "decide-destination"
                          , G_CALLBACK(decidedDestinationGC), THIS );
                          
  g_signal_connect( download, "received-data"
                                , G_CALLBACK(receivedDataGC), nullptr );
}



static
void
decidePolicyGC( WebKitWebView * view
              , WebKitPolicyDecision * decision
              , WebKitPolicyDecisionType type
              , WEBKIT2_engine * THIS
              )
{
  WebKitNavigationPolicyDecision * navigationDecision;
  WebKitNavigationAction * navigationAction;
  WebKitNavigationType navigationType;
  WebKitURIRequest * request;
  WebKitURIResponse * response;
  WebKitWebResource * mainResource;
  WebKitDownload * download;
  std::string uri;
  std::string mimeType;
  gint state;
  
  //~ std::cout << "decidePolicyGC: ";
  switch( type )
  {
    case WEBKIT_POLICY_DECISION_TYPE_NAVIGATION_ACTION:
    {// add them or fear the "jump to case label" error...
      //~ std::cout << "WEBKIT_POLICY_DECISION_TYPE_NAVIGATION_ACTION" << std::endl;
      
      navigationDecision = WEBKIT_NAVIGATION_POLICY_DECISION(decision);
      
      navigationAction = 
                 webkit_navigation_policy_decision_get_navigation_action 
                                                 ( navigationDecision );
      
      navigationType = webkit_navigation_action_get_navigation_type
                                                   ( navigationAction );

      switch( navigationType )                                           
      { 
        case WEBKIT_NAVIGATION_TYPE_LINK_CLICKED:
        {
          //~ std::cout << "WEBKIT_NAVIGATION_TYPE_LINK_CLICKED" << std::endl;
          state = webkit_navigation_action_get_modifiers(
                                                     navigationAction );
          if( state == GDK_CONTROL_MASK ) // CTRL + CLICK LEFT
          {
            request = webkit_navigation_action_get_request(
                                                     navigationAction );

            uri = webkit_uri_request_get_uri( request );
            
            THIS->addANewWebView( uri, false, view, true );
            
            webkit_policy_decision_ignore( decision );
          }
          else
            webkit_policy_decision_use( decision );
        } break;
        
        case WEBKIT_NAVIGATION_TYPE_FORM_SUBMITTED:
        {
          //~ std::cout << "WEBKIT_NAVIGATION_TYPE_FORM_SUBMITTED" << std::endl;
          webkit_policy_decision_use( decision );
        } break;
        
        case WEBKIT_NAVIGATION_TYPE_BACK_FORWARD:
        {
          //~ std::cout << "WEBKIT_NAVIGATION_TYPE_BACK_FORWARD" << std::endl;
          webkit_policy_decision_use( decision );
        } break;
        
        case WEBKIT_NAVIGATION_TYPE_RELOAD:
        {
          //~ std::cout << "WEBKIT_NAVIGATION_TYPE_RELOAD" << std::endl;
          webkit_policy_decision_use( decision );
        } break;
        
        case WEBKIT_NAVIGATION_TYPE_FORM_RESUBMITTED:
        {
          //~ std::cout << "WEBKIT_NAVIGATION_TYPE_FORM_RESUBMITTED" << std::endl;
          webkit_policy_decision_use( decision );
        } break;
        
        case WEBKIT_NAVIGATION_TYPE_OTHER:
        {
          //~ std::cout << "WEBKIT_NAVIGATION_TYPE_OTHER" << std::endl;
          webkit_policy_decision_use( decision );
        } break;
        
        default : // this should not happen.
        {
          std::cout << "unknown (navigation_type)" << std::endl;
          webkit_policy_decision_ignore( decision );
        } break;
      }      
    } break;

    case WEBKIT_POLICY_DECISION_TYPE_NEW_WINDOW_ACTION:
    {
      navigationDecision = WEBKIT_NAVIGATION_POLICY_DECISION(decision);
      
      navigationAction = 
                 webkit_navigation_policy_decision_get_navigation_action 
                                                 ( navigationDecision );
      
      navigationType = webkit_navigation_action_get_navigation_type
                                                   ( navigationAction );

      switch( navigationType )                                           
      { 
        case WEBKIT_NAVIGATION_TYPE_LINK_CLICKED:
        {
          //~ std::cout << "WEBKIT_NAVIGATION_TYPE_LINK_CLICKED" << std::endl;
          state = webkit_navigation_action_get_modifiers(
                                                     navigationAction );

          request = webkit_navigation_action_get_request(
                                                     navigationAction );

          uri = webkit_uri_request_get_uri( request );
          
          THIS->addANewWebView( uri, false, view );
          
          webkit_policy_decision_ignore( decision );
        } break;
        
        case WEBKIT_NAVIGATION_TYPE_FORM_SUBMITTED:
        {
          //~ std::cout << "WEBKIT_NAVIGATION_TYPE_FORM_SUBMITTED" << std::endl;
          webkit_policy_decision_use( decision );
        } break;
        
        case WEBKIT_NAVIGATION_TYPE_BACK_FORWARD:
        {
          //~ std::cout << "WEBKIT_NAVIGATION_TYPE_BACK_FORWARD" << std::endl;
          webkit_policy_decision_use( decision );
        } break;
        
        case WEBKIT_NAVIGATION_TYPE_RELOAD:
        {
          //~ std::cout << "WEBKIT_NAVIGATION_TYPE_RELOAD" << std::endl;
          webkit_policy_decision_use( decision );
        } break;
        
        case WEBKIT_NAVIGATION_TYPE_FORM_RESUBMITTED:
        {
          //~ std::cout << "WEBKIT_NAVIGATION_TYPE_FORM_RESUBMITTED" << std::endl;
          webkit_policy_decision_use( decision );
        } break;
        
        case WEBKIT_NAVIGATION_TYPE_OTHER:
        {
          //~ std::cout << "WEBKIT_NAVIGATION_TYPE_OTHER" << std::endl;
          webkit_policy_decision_use( decision );
        } break;
        
        default : // this should not happen.
        {
          //~ std::cout << "unknown (navigation_type)" << std::endl;
          webkit_policy_decision_ignore( decision );
        } break;
      }      
    } break;

    case WEBKIT_POLICY_DECISION_TYPE_RESPONSE:
    {
      //~ std::cout << "WEBKIT_POLICY_DECISION_TYPE_RESPONSE" << std::endl;
      WebKitResponsePolicyDecision * responseDecision = 
                              WEBKIT_RESPONSE_POLICY_DECISION(decision);

      response = webkit_response_policy_decision_get_response(
                                                      responseDecision);
                                                      
      mimeType = webkit_uri_response_get_mime_type(response);
      //~ std::cout << mimeType << std::endl;
      if( !webkit_response_policy_decision_is_mime_type_supported(
                                                      responseDecision ) 
          || mimeType == "application/pdf" )
      {
        //~ std::cout << "     download" << std::endl;
        WebKitWebContext* context = webkit_web_context_get_default();

        g_signal_connect( context, "download-started"
                                   , G_CALLBACK(downloadRequestedGC)
                                                            , THIS );
                                   
        webkit_policy_decision_download( decision );
      }
    } break;

    default: // this should not happen.
    {
      std::cout << "unknown (decidePolicyGC)" << std::endl;
      webkit_policy_decision_ignore( decision );
    } break;
  }
}

static
void
testSchemeRequestGC( WebKitURISchemeRequest * request
                   , gpointer userData
                   )
{
  GInputStream * stream;
  gsize stream_length;
  const gchar * path;

  path = webkit_uri_scheme_request_get_path( request );
  if( !g_strcmp0( path, "example" ) )
  {
    gchar * contents;

    contents = g_strdup_printf( "<html><body><p>Example about page</p>"
                                                     "</body></html>" );
    stream_length = strlen( contents );
    stream = g_memory_input_stream_new_from_data( contents, 
                                                stream_length, g_free );
  }
  else
  {
    std::cout << path << std::endl;
    return;
  }
  webkit_uri_scheme_request_finish (request,
                                    stream,
                                    stream_length,
                                    "text/html");
  g_object_unref (stream);
}

static //*************************************************** [FINISH ME]
void
scriptDialogGC( WebKitWebView * view
              , WebKitScriptDialog * dialog
              , gpointer data
              )
{
  //~ std::cout << "scriptDialogGC" << std::endl;
  WebKitScriptDialogType type = 
                         webkit_script_dialog_get_dialog_type( dialog );
  // must split the different dialog and make a window with button(s)...
  // see, 
  // http://webkitgtk.org/reference/webkit2gtk/1.9.4/WebKitWebView.html#webkit-script-dialog-get-message
  // and,
  // webkit_script_dialog_confirm_set_confirmed ()
  if( type == WEBKIT_SCRIPT_DIALOG_ALERT
      || type == WEBKIT_SCRIPT_DIALOG_PROMPT
      || type == WEBKIT_SCRIPT_DIALOG_CONFIRM )
    std::cout << webkit_script_dialog_get_message( dialog ) << std::endl;
  else
    std::cout << "scriptDialogGC, type unknown!" << std::endl;
}


WEBKIT2_engine::WEBKIT2_engine( Application* appli
                              )
: engine(appli)
, matched(false)
, webKitWebContext(webkit_web_context_get_default())
, defaultURI( "https://bitbucket.org/nicooplusplus/eclaireur/downloads" /*"https://duckduckgo.com/"*/ )
{
  // type "test:example" in the entry
  webkit_web_context_register_uri_scheme( webKitWebContext, "test",
                                testSchemeRequestGC, nullptr, nullptr );
}

WEBKIT2_engine::~WEBKIT2_engine(  )
{
}

std::string
WEBKIT2_engine::checkURI( const std::string uri )
{
  //TODO check if it's a local file*************************************
  std::string prefix1 = "http://";
  std::string prefix2 = "https://";
  std::string prefix3 = "www.";
  if( uri.substr( 0, prefix1.size() ) == prefix1 
      || uri.substr( 0, prefix2.size() ) == prefix2 )
  {
    return uri;
  }
  if(uri.substr(0, prefix3.size()) == prefix3)
  {
    return "http://" + uri;
  }
  return "http://www." + uri;
  
}

void
WEBKIT2_engine::loadURI( const std::string uri
                       , int ID )
{
  webkit_web_view_load_uri( WebViewsList[ID], checkURI( uri ).c_str() );
}

void
WEBKIT2_engine::loadURI( const std::string uri
                       , WebKitWebView* view )
{
  webkit_web_view_load_uri( view, checkURI( uri ).c_str() );
}

void
WEBKIT2_engine::goBack( int ID )
{
  webkit_web_view_go_back( WebViewsList[ID] );
  
}

void
WEBKIT2_engine::goForward( int ID )
{
  webkit_web_view_go_forward( WebViewsList[ID] );
}

void
WEBKIT2_engine::search( const char * string_to_search
                      , int ID )
{
  findController = webkit_web_view_get_find_controller( WebViewsList[ID] );
  g_signal_connect ( findController, "found-text",
                                   G_CALLBACK (foundTextGC), &matched );
  webkit_find_controller_search( findController, string_to_search, WEBKIT_FIND_OPTIONS_NONE /*enum WebKitFindOptions*/ , 1000);
}

void
WEBKIT2_engine::endSearch(
                         )
{
  if( matched )
  {
    webkit_find_controller_search_finish( findController );
    matched = false;
  }
}

bool
WEBKIT2_engine::matching(
                        )
{
  return matched;
}

void
WEBKIT2_engine::previousFound(
                             )
{
  webkit_find_controller_search_previous( findController );
}

void
WEBKIT2_engine::nextFound(
                         )
{
  webkit_find_controller_search_next( findController );
}

void
WEBKIT2_engine::addANewWebView( std::string uri
                              , bool simpleViewer
                              , WebKitWebView * view
                              , bool focus
                              )
{
  view =  WEBKIT_WEB_VIEW( webkit_web_view_new() );
  if( uri == "" )
    uri = defaultURI;
  // see signals :
  //  http://webkitgtk.org/reference/webkit2gtk/1.9.4/WebKitWebView.html
  if( !simpleViewer )
  {
    g_signal_connect( view, "load-changed"
                                    , G_CALLBACK(loadChangedGC), this );
  
    g_signal_connect( view, "notify::title"
                                    , G_CALLBACK(notifyTitleGC), this );
    g_signal_connect( view, "decide-policy"
                                   , G_CALLBACK(decidePolicyGC), this );
    g_signal_connect( view, "script-dialog"
                                , G_CALLBACK(scriptDialogGC), nullptr );
    // SIMPLE VIEWER SCHOULD NOT DOWNLOAD ANYTHING!!
    //~ g_signal_connect( view, "download-requested",
                             //~ G_CALLBACK(downloadRequestedGC), nullptr );
  }
                                  
  WebViewsList.push_back( view );
  InvertedWebViewList[view] = InvertedWebViewList.size()-1;

  eGTKMM->addWebkit( GTK_WIDGET( view ), focus );
    
  loadURI( uri, view ); // order is important!
}
