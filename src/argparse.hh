/*
 * argparse.hh
 * 
 * Copyright 2013-2015 Jäger Nicolas
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
#ifndef __ARGPARSE_H_INCLUDED__
#define __ARGPARSE_H_INCLUDED__

#include <iostream>
#include <vector>
#include <map>
#include <string>

struct InitData
{
  int width = 640, height = 480;
  std::string URI    = "";
  bool fullscreen    = false;
  bool simpleViewer  = false;
};

class Argparse
{
  public:
	Argparse(int, char**);
  bool error();
  bool help();
  InitData* dataParsed();
  
  private:
  InitData initData;
  bool ERROR = false;
  bool HELP = false;
};


#endif // __ARGPARSE_H_INCLUDED__
