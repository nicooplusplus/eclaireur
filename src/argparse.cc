/*
 * argparse.cc
 * 
 * Copyright 2013-2015 Jäger Nicolas
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "argparse.hh"
/**
 * parses the command line and store the argument into data
 */
Argparse::Argparse( int argc
                  , char **argv
                  )
{
	std::vector<std::string> command_line;
	for (int i=1; i<argc; i++)
		command_line.push_back(std::string(argv[i]));

  initData.URI = "";

	// parsing
  for( auto i = command_line.begin(); i != command_line.end(); i++ )
  {
    if( *i == "--URI" || *i == "-u" )
    {
      initData.URI = *(++i);
    }
		else if( *i == "--help" || *i == "-h" )
    {
      HELP = true;
    }
    else if ( *i == "--simpleViewer" || *i == "-v" )
    {
      initData.simpleViewer = true;
    }
		else if ( *i == "--fullscreen" || *i == "-fs" )
    {
      initData.fullscreen = true;
    }
    else if( *i == "--windowed" || *i == "-w" )
    {
      if( i == command_line.end() )
        ERROR = true;
      else
      {
        // TODO record the width and height ****************************
        std::cout << *(++i) << std::endl; 
        std::cout << *i << std::endl; 
          if( i == command_line.end() )
          {
            ERROR = true;
          }
          else
          {
            std::cout << *(++i) << std::endl; 
          }
      }
    }
    else
    {
      ERROR = true; // unknown argument.
    }
	}
}

bool
Argparse::help(
              )
{
  if( HELP )
  {
    std::cout << "« Eclaireur »\n"
              << " --help, -h : this window\n"
                 //" --fullscreen, -fs : fullscreen mode\n"
              << " --URI, -u : set the url to open\n"
              << " --simpleViewer, -v : just a window\n";
              
    return true;
  }
  else
  {
    return false;
  }
}

bool
Argparse::error(
               )
{
  if( ERROR )
  {
    std::cout << "Bad argument(s), use \"--help\" or \"-h\" to see all possible arguments.\n";
    return true;
  }
  else
  {
    return false;
  }
}

InitData*
Argparse::dataParsed(
                    )
{
  return &initData;
}
