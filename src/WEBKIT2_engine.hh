#ifndef __WEBKIT2_ENGINE_H_INCLUDED__
#define __WEBKIT2_ENGINE_H_INCLUDED__
#include "engine.hh"

class
WEBKIT2_engine : public engine
{
  public:
  WEBKIT2_engine(Application*);
  ~WEBKIT2_engine();
  inline GtkWidget* getWebViewGTKWidget(int ID) 
                             { return GTK_WIDGET( WebViewsList[ID] ); };
  inline WebKitWebView* getWebView(int ID) { return WebViewsList[ID]; };
  inline int getWebViewIndex(WebKitWebView* W)
                                     { return InvertedWebViewList[W]; };
  void loadURI(const std::string,int);
  void loadURI(const std::string,WebKitWebView*);
  void goBack(int);
  void goForward(int);
  void search(const char*,int);
  void endSearch();
  bool matching();
  void previousFound();
  void nextFound();
  void addANewWebView(std::string, bool = false
             ,WebKitWebView* = WEBKIT_WEB_VIEW( webkit_web_view_new())
             , bool = false );
                                                         
  
  private:
  std::vector <WebKitWebView*> WebViewsList;
  std::map <WebKitWebView*, int> InvertedWebViewList;
  WebKitFindController* findController;
  bool matched;
  WebKitWebContext* webKitWebContext;
  std::string checkURI(const std::string);
  const std::string defaultURI;

};

#endif // __WEBKIT2_ENGINE_H_INCLUDED__
