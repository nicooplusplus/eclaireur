/*
 * application.cc
 * 
 * Copyright 2013-2015 Jäger Nicolas
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
#include "WEBKIT2_engine.hh"
#include "GTKMM_engine.hh"
#include "application.hh"

Application::Application( const InitData* _data
                        )
: parsed(_data)
{
  eGTKMM = new GTKMM_engine( this, parsed->simpleViewer );
  eWEBKIT2 = new WEBKIT2_engine( this );
  width  = parsed->width;
  height = parsed->height;
  // bind engines
  eGTKMM->attach_WEBKIT2_engine( eWEBKIT2 );
  eWEBKIT2->attach_GTKMM_engine( eGTKMM );
  eWEBKIT2->addANewWebView( parsed->URI, parsed->simpleViewer );
}


void Application::run(
                     )
{
  eGTKMM->startUI();
}

Application::~Application(
                         )
{
  delete eGTKMM;
	delete eWEBKIT2;
}

int Application::getWidth(
                         )
{
  return width;
}

int Application::getHeight(
                          )
{
  return height;
}

void Application::setResolution( const int w
                               , const int h
                               )
{
  width = w ; height = h;
}
