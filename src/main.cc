/*
 * main.cc
 * 
 * Copyright 2013-2014 Jäger Nicolas
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
#include <unistd.h>
#include <iostream>
#include "argparse.hh"
#include "application.hh"

int main( int argc
        , char **argv )
{
  if( geteuid() == 0 )
  {
    std::cerr<<"ABORTED: Attempted to run with root privileges!\n";
    return EXIT_FAILURE;
  }
  
  std::cout << "- youtube sucks!\n"
            << "- key pressed/released have events! check GTKMM_engine::onKeyPressed.\n"
            << "- set the width and height trough the arguments of command line.\n"
            << "- javascript dialogs need more work (scriptDialogGC) .\n"
            << "- check the strange '+' bug with search engine...\n"
            << "- WORKING ON DOWNLOAD OPERATIONS.\n"
            << "- defaultURI has to be in a config file.\n"
            << "- search in the page, case sensitive ON/OFF.\n"
            << "- FCW is a nullptr!\n"
            << "- strategy needed for the case we hit a key and clicked a button at same time.\n"
            << "- search engine, hardcoded : std::string searchEngineURI = \"https://duckduckgo.com/?q=\";\n"
            << std::endl;

  Argparse ap( argc, argv ); // parse the arguments.
  if( ap.error() )           // true if at least one arguments is unknown.
    return EXIT_FAILURE;
  if( ap.help() )            // if true, show help message and exit.
    return EXIT_SUCCESS;
  Application application( ap.dataParsed() );
  application.run();

  std::cout << "\nsee you soon! ;)\n";
  return EXIT_SUCCESS;
}
