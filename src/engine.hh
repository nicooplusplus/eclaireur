/*
 * engine.hh
 * 
 * Copyright 2013-2015 Jäger Nicolas
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
#ifndef __ENGINE_H_INCLUDED__
#define __ENGINE_H_INCLUDED__

#include <map> // needed for engine_event
#include <queue> 
#include <boost/thread/thread.hpp>

#include <gtkmm.h>
#include <webkit2/webkit2.h>

class GTKMM_engine;
class WEBKIT2_engine;
class Application;

/**
* generic class to manage events between engines
*/

class engine_event
{
public:
	int type;
	std::map<std::string, std::string> s_data;
	std::map<std::string, int> i_data;

	bool operator==(const engine_event& e)
  {
		return (type == e.type) 
        && (s_data == e.s_data) 
			  && (i_data == e.i_data);
	}

	template<class Archive>
	void serialize(Archive& ar, const unsigned int)
  {
		ar & type;
		ar & s_data;
		ar & i_data;
	}
};


class engine
{
public:
	engine(Application*);
	virtual ~engine();
  
	inline Application*		 get_parent()			   {return parent;}
	inline GTKMM_engine*	 get_GTKMMengine()	 {return eGTKMM;}
  inline WEBKIT2_engine* get_WEBKIT2engine() {return eWEBKIT2;   }

  void attach_GTKMM_engine(GTKMM_engine *e)     {eGTKMM   = e;}
  void attach_WEBKIT2_engine(WEBKIT2_engine *e) {eWEBKIT2 = e;} 
  
protected:
  GTKMM_engine   *eGTKMM;
	WEBKIT2_engine *eWEBKIT2;
	Application *parent; // pointer to container object
  
};

#endif // __ENGINE_H_INCLUDED__
