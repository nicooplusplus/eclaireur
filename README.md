Licence :
=========
  Copyright 2015 Jäger Nicolas
 
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  MA 02110-1301, USA.

Overview :
==========
 ***«Eclaireur»*** is a small web browser. It's written for working on
 **linux**. 

Dependecies :
==============
  - gtkmm
  - Webkit2GTK+
  - Boost

Compil :
========
  - $ cmake .
  - $ make

Known bug :
============
  - opening a video on youtube crashs the Webkit viewer.
  
About the name :
================
  *«Eclaireur»* means scout in french.
